
sudo apt-get install unzip

#Create the directory and cd into it
sudo mkdir /opt/cura
cd /opt/cura

#Get the archive
sudo cp /opt/ins/assets/cura.zip /opt/cura/cura.zip
unzip *

#Create the shortcut
cp cura.desktop ~/.local/share/applications/cura.desktop
