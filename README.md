# INS package manager

Very easy and fast package manager. For beginner and more.

## Features

* Support SH scripts (INS scripts)
* Support APT
* *Will support Snap (Not implemented yet)*
* Support search of APT packages
* Nice built-in scripts to install common software and less commons one in one command. (more information bellow)

## Install

1. Open in terminal
2. Copy and paste the following command:

```shell
sudo apt-get install -y git && git clone https://gitea.com/chopin42/ins && cd ins && sudo sh setup.sh
```

## Usage

| Goal                         | Usage            | Example                                                      |
| ---------------------------- | ---------------- | ------------------------------------------------------------ |
| Install official APT         | `ins i NAME`     | `ins i aptitude`                                             |
| Install unofficial APT       | `ins i PPA NAME` | `ins i obsproject/obs-studio obs-studio`                     |
| Install remote shell scripts | `ins i URL`      | `ins i https://gitea.com/chopin42/mygist/raw/branch/master/script.sh` |
| Install built-in script      | `ins i NAME`     | `ins i brave-browser`                                        |
| Search for APT               | `ins s NAME`     | `ins s lib`                                                  |
| Remove package               | `ins r NAME`     | `ins r obs-studio`                                           |

## List of built-in apps

There are some built-in scripts in INS. This is a list of packages that you can install in one command using INS (plus all the official APTs) :

| Packages                | Descriptions                                                 |
| ----------------------- | ------------------------------------------------------------ |
| `bonus`                 | Bonus apps for your computer                                 |
| `brave-browser-beta`    | Brave Browser Beta                                           |
| `brave-browser-nightly` | Brave Browser Unstable Nightly                               |
| `brave-browser`         | Secure, fast and private web browser with Tor                |
| `cura`                  | 3D printers that simply work                                 |
| `discord`               | A new way to chat with your communities and friends          |
| `gaming`                | A gaming package for Windows gamers that are on Linux :fire: |
| `hyper`                 | The hyper.is terminal for a better experience!               |
| `mcss`                  | Unstable Minecraft server creator                            |
| `minecraft-offline`     | *Cracked* Minecraft in one command, but be quiet!            |
| `obs-studio`            | Powerful Free and open source software for video recording and live streaming. |
| `openscad`              | The Programmer's Solid 3D CAD Modeller                       |
| `typora`                | A truly minimal markdown editor                              |
| Your app ?              |                                                              |
|                         |                                                              |

## Contribute

### Create or implement a new software for INS (built-in)

1. Fork this repository
2. Add a new sh file to add your software's installation script
3. Make a pull request and present your software. To avoid conflicts you do not need to add a new line in this README.
4. Add the setup informations on your official website.

### Support new sources

In the future several sources other than APT and INS will be supported, like the following ones:

*  Snappy
* Homebrew
* FlatPak

If you can integrate them that would be a very good contribution :smile:

### Issues

You can also post an issue or answer to new ones, or implementing some using a pull request.

## License

This project is under the GNU-GPL-V3
